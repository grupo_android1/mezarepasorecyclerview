package com.meza.repasorecycler;

import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adaptador extends RecyclerView.Adapter <Elemento>{

    List<String> elementos;

    public Adaptador(List<String> datos){

        elementos=datos;
    }


    @NonNull
    @Override
    public Elemento onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View miCajon;
        miCajon=LayoutInflater.from(parent.getContext()).inflate(R.layout.elemento,parent, false);

        return new Elemento(miCajon);

    }

    @Override
    public void onBindViewHolder(@NonNull Elemento holder, int position) {

        String miDato= elementos.get(position);
        holder.tvElemento.setText(miDato);

    }

    @Override
    public int getItemCount() {
        if(elementos!=null) {
            return elementos.size();
        }else{
            return 0;
        }

    }
}
