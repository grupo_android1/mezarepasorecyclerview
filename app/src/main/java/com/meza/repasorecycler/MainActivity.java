package com.meza.repasorecycler;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DownloadManager;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView rvElementos;
    Adaptador miAdaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //tenemos el recycler view
        rvElementos=findViewById(R.id.rvElementos);
        rvElementos.setLayoutManager(new LinearLayoutManager(this));

        //definir datos puden venir de muchos sitios

        getElementos();
        //datos definidos locales:
        List<String> misDatosLocales=new ArrayList<>();
        misDatosLocales.add("carlos");
        misDatosLocales.add("Mario");
        misDatosLocales.add("luis");
        misDatosLocales.add("Alvaro");

        //tomar los datos de una base de datos local/remota


        //tomar  desde los recursos tipos de stringarray
        List<String> misDatosString= Arrays.asList(getResources().getStringArray(R.array.nombres));

        //crear un objeto de la clase adaptador
        miAdaptador=new Adaptador(misDatosLocales);
        rvElementos.setAdapter(miAdaptador);

        //llamar la funcion getElementos


    }

    public void getElementos(){
        String miUrl="https://www.balldontle.io/api/v1/teams";

        RequestQueue miPila = Volley.newRequestQueue(this);

        //Realizar la peticion
        JsonObjectRequest miPeticion;

        //Inicializar el objeto como una peticion
        miPeticion =new JsonObjectRequest(Request.Method.GET, miUrl, null,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Obtener los datos que entrega la API
                        try {

                            //cargar el string fullname de cada uno de los equipos
                            JSONArray miArreglo = response.getJSONArray("data");
                            List<String> elementos = new ArrayList<>();

                            for (int i = 0; i < miArreglo.length(); i++) {
                                JSONObject miElemento = miArreglo.getJSONObject(i);
                                elementos.add(miElemento.getString("full_name"));
                            }

                            //mostrar los resultados en el recycler view
                            RecyclerView rv = findViewById(R.id.rvElementos);
                            miAdaptador = new Adaptador(elementos);
                            rv.setAdapter(miAdaptador);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Hubo un error");
            }
        });
        miPila.add(miPeticion);
    }
}