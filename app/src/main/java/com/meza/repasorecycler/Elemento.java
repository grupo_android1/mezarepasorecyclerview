package com.meza.repasorecycler;


import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

//Esta clase es un voewholder(cajon para cargar datos en un recycler view)
public class Elemento extends RecyclerView.ViewHolder {

    TextView tvElemento;

    //constructor - permite pasar el layout
    public Elemento(@NonNull View itemView) {
        super(itemView);

        tvElemento=itemView.findViewById(R.id.textElemento);

    }
}
